/*
 * Copyright (C) 2018 rhn <gihu.rhn@porcupinefactory.org>
 *
 * SPDX-License-Identifier: GPL-3+
 */

use std::cell::RefCell;
use std::collections::HashMap;
use std::convert::From;
use std::fmt;
use std::fs::File;
use std::io;
use std::io::{ BufRead, BufReader, Read };
use std::path::PathBuf;
use std::rc::Rc;
use std::vec::Vec;

use ::chrono;
use ::mailparse::{ parse_mail, ParsedMail, parse_headers, MailHeader, MailParseError };
use ::ccell::CachedCell;


pub enum MessageId {
    Good(String),
    Bad(String),
}

fn parse_msg_id(header: &str) -> MessageId {
    let header = header.trim();
    if header.starts_with('<') && header.ends_with('>') {
        MessageId::Good(header[1 .. header.len() - 1].into())
    } else {
        MessageId::Bad(header.into())
    }
}


type Date = chrono::DateTime<chrono::FixedOffset>;

#[derive(Clone)]
pub enum MessageDate {
    Good(Date),
    Bad(String),
    Missing
}

/// A special printer for MessageDate skipping the timezone
pub struct NaiveMessageDate(MessageDate);

impl MessageDate {
    fn fmt_with_opts(&self, f: &mut fmt::Formatter, with_tz: bool) -> fmt::Result {
        match self {
            MessageDate::Good(d) => write!(f, "{}", d.format(
                if with_tz {
                    "%a %F %T %z"
                } else {
                    "%a %F %T"
                }
            )),
            MessageDate::Bad(s) => write!(f, "Bad: {}", s),
            MessageDate::Missing => write!(f, "Missing"),
        }
    }

    pub fn into_utc(self) -> NaiveMessageDate {
        NaiveMessageDate(match self {
            MessageDate::Good(d) => MessageDate::Good(
                d.with_timezone(&chrono::FixedOffset::east(0))
            ),
            MessageDate::Bad(s) => MessageDate::Bad(s),
            MessageDate::Missing => MessageDate::Missing,
        })
    }
}

impl From<Option<String>> for MessageDate {
    fn from(value: Option<String>) -> Self {
        match value {
            Some(s) => match Date::parse_from_rfc2822(&s) {
                Ok(d) => MessageDate::Good(d),
                Err(_) => MessageDate::Bad(s)
            },
            None => MessageDate::Missing
        }
    }
}

impl fmt::Display for MessageDate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.fmt_with_opts(f, true)
    }
}

impl fmt::Display for NaiveMessageDate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt_with_opts(f, false)
    }
}

rental!{
    mod rentals {
        use super::*;
        #[rental]
        pub struct OwningMail {
            buffer: Vec<u8>,
            mail: ParsedMail<'buffer>
        }
    }
}

/// Stores frequently used headers in a processed form
#[derive(Clone)]
pub struct MessageMeta {
    pub msg_id: Option<String>,
    pub subject: Option<String>,
    pub date: MessageDate,
    pub sender: Option<String>,
    pub recipients: Option<String>, // FIXME: split To/CC/BCC
}

impl MessageMeta {
    pub fn new(
        msg_id: Option<MessageId>,
        subject: Option<String>, date: Option<String>,
        sender: Option<String>, recipients: Option<String>
    ) -> MessageMeta {
        MessageMeta {
            msg_id: msg_id.map(|mid| {
                match mid {
                    MessageId::Good(s) => s,
                    MessageId::Bad(s) => format!("Bad: {}", s),
                }
            }),
            subject: subject,
            date: date.into(),
            sender: sender,
            recipients: recipients,
        }
    }
}

pub struct CachedMessage {
    pub meta: MessageMeta,
    pub raw_headers: String, // TODO: does it really belong here? Maybe raw data should be in its own struct
    body: CachedCell<String>,
    mail: rentals::OwningMail,
}

impl CachedMessage {
    pub fn new(path: PathBuf) -> Result<Self, io::Error> {
        let mut buf = Vec::new();
        File::open(&path)?.read_to_end(&mut buf)?;
        let mailown = rentals::OwningMail::new(buf,
            |buf| parse_mail(&buf).expect("bad mail")
        );
        let (id, date, sender, subject, recipients) = mailown.rent(|mail| (
            header_find_key(&mail.headers, "Message-Id"),
            header_find_key(&mail.headers, "Date"),
            header_find_key(&mail.headers, "From"),
            header_find_key(&mail.headers, "Subject"),
            header_find_key(&mail.headers, "To"),
        ));
        Ok(CachedMessage {
            meta: MessageMeta::new(
                id.map(|i| parse_msg_id(&i)),
                subject,
                date,
                sender,
                recipients
            ),
            raw_headers: read_headers(path)?,
            body: CachedCell::new(),
            mail: mailown,
        })
    }
    
    pub fn get_body(&self) -> &str {
        match self.body.get() {
            None => {
                let body = self.mail.rent(|mail| {
                    fold_multipart(mail, Vec::new(),
                        &|msg: &ParsedMail| {
                            "inline" == header_find_key(&msg.headers, "Content-Disposition")
                                .or(Some("inline".into())).unwrap()
                        },
                        &|mut init, msg| {
                            init.push(msg.get_body().expect("bad body"));
                            init
                        }
                    )
                }).join("\n\n"); // FIXME: this body is good for presentation, not as data source
                self.body.set(body.clone())
            },
            Some(value) => value,
        }
    }
}

fn header_find_key(headers: &[MailHeader], key: &str) -> Option<String> {
    headers.iter()
        .find(|h| {
            h.get_key()
                .unwrap_or_else(|e| {
                    eprintln!("Header key fail: {}", e);
                    String::new()
                }).to_lowercase()
                == key.to_lowercase()
        })
        .and_then(|h| {
            h.get_value()
                .map(Some)
                .unwrap_or_else(|e| {
                    eprintln!("Header value fail: {}", e);
                    None
                })
        })
}

fn fold_multipart<T:>(message: &ParsedMail, init: T,
                      pred: &dyn Fn(&ParsedMail) -> bool,
                      fold: &dyn Fn(T, &ParsedMail) -> T,
) -> T {
    if !message.ctype.mimetype.starts_with("multipart/") {
        if !pred(message) {
            init
        } else {
            fold(init, message)
        }
    } else {
        message.subparts.iter().fold(init,
            |init, item| fold_multipart(item, init, pred, fold))
    }
}

#[derive(Debug)]
pub enum MessageRetrievalError {
    Io(io::Error),
    Parse(MailParseError),
}

impl From<io::Error> for MessageRetrievalError {
    fn from(e: io::Error) -> Self {
        MessageRetrievalError::Io(e)
    }
}

impl From<MailParseError> for MessageRetrievalError {
    fn from(e: MailParseError) -> Self {
        MessageRetrievalError::Parse(e)
    }
}

/// Information passed to the display model
#[derive(Clone)]
pub struct DisplayMessageMeta {
    /// A message ID, to match messages against queries for model updating purposes
    pub id: String,
    /// The path to the message representation
    pub path: PathBuf,
    /// Header cache; a lack of a result means that it hasn't been queried, but an empty result is lack of a header
    headers: Rc<RefCell<HashMap<String, Option<String>>>>, // TODO for speed: there's going to be <10 headers used, just replace with a vector or a hardcoded struct once they are known
    pub depth: u32,
    /// Message subject will be decorated with various "RE:"'s, so thread's version stays to track the thread
    pub thread_subject: String,
    // Notmuch properties
    pub unread: bool // TODO: tags?
}

impl DisplayMessageMeta {
    pub fn new(id: String, path: PathBuf, depth: u32, thread_subject: String, unread: bool) -> Self {
        DisplayMessageMeta {
            id,
            path,
            headers: Rc::new(RefCell::new(HashMap::new())),
            depth,
            thread_subject,
            unread,
        }
    }
    
    pub fn get_date(&self) -> MessageDate {
        self.get_header("date")
            .unwrap_or_else(|e| {
                eprintln!("Failed to parse headers: {:?}", e);
                None
            })
            .into()
    }
    
    /// Returns an ID that can be used to match it with messages returned from queries
    pub fn get_id(&self) -> String {
        self.id.clone()
    }

    /// Loads all headers until a read error occurs, the header is found, or the headers end.
    /// Parsing errors in individual headers will be ignored.
    /// FIXME: Will consume /dev/urandom if newlines are stripped
    pub fn get_header(&self, name: &str) -> Result<Option<String>, MessageRetrievalError> {
        let name = name.to_lowercase();
        if let Some(hdr) = self.headers.borrow().get(&name) {
            return Ok(hdr.clone());
        }
        
        let header_data = read_headers(self.path.clone())?;

        let parsed_headers = parse_headers(header_data.as_bytes())?.0;
        let parse_results = parsed_headers.into_iter()
            .map(|parsed_header_res| -> Result<_, MailParseError> {
                Ok((
                    parsed_header_res.get_key()?.to_lowercase(),
                    // TODO: what to do with invalid headers? Are they worth saving, considering that all that will happen is additional file hits with additional calls?
                    parsed_header_res.get_value()?,
                ))
            });

        let mut found_value = None;
        
        let mut header_map = self.headers.borrow_mut();
        for parse_result in parse_results {
            match parse_result {
                Ok((key, value)) => {
                    header_map.insert(key.clone(), Some(value.clone()));
                    if name == key {
                        found_value = Some(value);
                    }
                },
                Err(e) => eprintln!("Header unparsed in file {:?}. Reason {}", self.path, e),
            }
        }
        Ok(found_value)
    }
}

fn read_headers(path: PathBuf) -> Result<String, io::Error> {
    let mut header_data = String::new();
    for lres in BufReader::new(File::open(path)?).lines() {
        let l = lres?;
        if l.is_empty() {
            break;
        } else {
            header_data.push_str(l.as_str());
            header_data.push_str("\r\n");
        }
    }
    Ok(header_data)
}

pub struct MessageRecord {
    pub unread: bool,
    pub inbox: bool,
}
