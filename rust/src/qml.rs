/*
 * Copyright (C) 2018, 2019 rhn <gihu.rhn@porcupinefactory.org>
 *
 * SPDX-License-Identifier: GPL-3+
 */

#[allow(unused_variables)]

use std::cell::RefCell;
use std::collections::HashSet;
use std::convert::From;
use std::env;
use std::rc::Rc;
use std::sync::{ Arc, Mutex };
use std::vec::Vec;
use std::path::{ Path, PathBuf };

use ::notmuch;
use ::notmuch::{Message, MessageOwner };

use interface::*;
use data;
use db;
use db::ApplyResult;
use persistence;

use utils::{try_or_else, try_or_else_none};
use utils::CombinedSlice;
use utils::ExactSize;


pub struct Storage {
    emit: StorageEmitter,
    base_path: String,
    base: Rc<RefCell<Database>>,
    bookmarks_data: Rc<RefCell<BookmarksData>>,
}

impl StorageTrait for Storage {
    fn new(emit: StorageEmitter) -> Self {
        let base_path = env::var("MAILDIR").expect("No MAILDIR env variable, cannot scan");
        let scanner = Rc::new(RefCell::new(db::ChangeScanner::new(base_path.clone())));
        let base = Database::create(DatabaseArgs::new(&base_path, scanner));
        Storage {
            emit: emit,
            base_path: base_path,
            base: base,
            bookmarks_data: BookmarksData::create(persistence::Bookmarks::create_or_load().unwrap()),
        }
    }
    fn emit(&mut self) -> &mut StorageEmitter {
        &mut self.emit
    }
    fn database(&self) -> &Database {panic!();}
    fn database_mut(&mut self) -> Option<&mut Rc<RefCell<Database>>> {
        Some(&mut self.base)
    }

    fn set_database(&mut self, _nmdatabase: Rc<RefCell<Database>>) {
        panic!("Changing databases from outside not supported");
    }
    fn clear_database(&mut self) {
        panic!("Changing databases from outside not supported");
    }
    
    /// Synchronizes NMDatabase state with messages present in storage.
    fn update(&mut self) {
        let db = match db::open_or_create(&self.base_path) {
            Err(e) => {
                eprintln!(
                    "Failed to open a Notmuch database in path {}: {}",
                    &self.base_path,
                    e
                );
                return;
            },
            Ok(db) => db,
        };
        // TODO: Find messages that changed     
        let (_db, e, changes) = db::with_new_messages(db, Path::new(&self.base_path));
        if let Some(e) = e {
            eprintln!("Errors encountered while searching for messages: {:?}", e);
        }

        if let Some(changes) = changes {
            // There are 2 database instances now, until all the references held by other objects are gone. This is probably not safe.
            // TODO: make the old database inert
            let old_scanner = {
                let old_base = self.base.borrow();
                let old_args = old_base.data.borrow();
                old_args.scanner.clone()
            };
            self.base = Database::create(DatabaseArgs::new(&self.base_path, old_scanner));
            {
                let mut base = self.base.borrow_mut();
                let emit = base.emit.clone();
                let base = base;
                let args = base.data.borrow();
                let scanner = args.scanner.borrow();
                scanner.reset();
                scanner.set_callback(Some(Box::new(move || emit.dirty_changed())));
            }
            self.emit.database_changed();
        }
    }
    
    fn bookmarks_data(&self) -> &BookmarksData {panic!()}
    fn bookmarks_data_mut(&mut self) -> Option<&mut Rc<RefCell<BookmarksData>>> {
        Some(&mut self.bookmarks_data)
    }
    fn set_bookmarks_data(&mut self, _value: Rc<RefCell<BookmarksData>>) {
        panic!("Not supported");
    }
    fn clear_bookmarks_data(&mut self) {
        panic!("Not supported");
    }

}

pub struct DatabaseArgs {
    // TODO: add maildir path, or IMAP server, or...        // This path should get replaced with something tracking usage of mutable and immutable references, and invalidating the latter after the former get released. Therefore, an env var is wrapped in an Arc<Mutex> already.
    path: Arc<Mutex<String>>,
    scanner: Rc<RefCell<db::ChangeScanner>>,
    // Should really be a signal
    last_changed_message_id: String,
}

impl DatabaseArgs {
    fn new(path: &str, scanner: Rc<RefCell<db::ChangeScanner>>) -> DatabaseArgs {
        DatabaseArgs {
            path: Arc::new(Mutex::new(path.to_string())),
            scanner,
            last_changed_message_id: String::new()
        }
    }
}

impl Database {
    /// Tries to apply the operation,
    /// and logs poison errors but doesn't let them win.
    fn apply_generic<'a, F, T>(&'a self, mut f: F, mode: notmuch::DatabaseMode)
        -> ApplyResult<T>
        where F: FnMut(&mut notmuch::Database) -> T
    {
        let data = self.data.borrow();
        let path = data.path.lock().unwrap_or_else(|e| {
            eprintln!("Database on Messages poisoned");
            e.into_inner()
        }).clone();
        let db = notmuch::Database::open(&path, mode);
        use self::ApplyResult::*;
        match db {
            Err(e) => ErrOpen(e),
            Ok(mut db) => {
                let res = f(&mut db);
                match db.close() {
                    Ok(()) => Success(res),
                    Err(e) => ErrClose(res, e),
                }
            },
        }
    }
    fn _apply<'a, F, R>(&'a self, mut f: F, mode: notmuch::DatabaseMode)
        -> Result<R, db::LockError<'a, Option<String>>>
        where F: FnMut(&notmuch::Database) -> Result<R, db::Error>
    {
        let data = self.data.borrow();
        let path = data.path.lock().map_err(|e| db::Error::TextError("Database on Messages poisoned"))?;
        let db = db::open(path.clone(), mode)?;                    
        let res = Ok(f(&db)?);
        if let notmuch::DatabaseMode::ReadWrite = mode {
            db.close().map_err(db::Error::from)?;
        }
        res
    }
    
    fn get<'a>(&'a self) -> Result<notmuch::Database, db::LockError<'a, Option<String>>> {
        let data = self.data.borrow();
        let path = data.path.lock().map_err(|e| db::Error::TextError("Database on Messages poisoned"))?;
        db::open(path.clone(), notmuch::DatabaseMode::ReadOnly)
            .map_err(db::LockError::Error)
    }
    
    fn apply<'a, F, R>(&'a self, f: F) -> Result<R, db::LockError<'a, Option<String>>>
        where F: FnMut(&notmuch::Database) -> Result<R, db::Error>
    {
        self._apply(f, notmuch::DatabaseMode::ReadOnly)
    }
    
    fn apply_mut<'a, F, R>(&'a self, f: F) -> Result<R, db::LockError<'a, Option<String>>>
        where F: FnMut(&notmuch::Database) -> Result<R, db::Error>
    {
        self._apply(f, notmuch::DatabaseMode::ReadWrite)
    }
    
    fn emit_message_change(&self, msg_id: String) {
        {
            let mut data = self.data.borrow_mut();
            data.last_changed_message_id = msg_id;
        }
        self.emit.changed_message_id_changed();
    }
}

impl DatabaseTrait for Database {
    fn emit(&self) -> &DatabaseEmitter {
        &self.emit
    }
    fn get_message(&self, msg_id: String) -> Rc<RefCell<MessageRecord>> {
        MessageRecord::create(
            try_or_else_none(
                || self.apply(
                    |db| db::fetch_record(&db, msg_id.as_str())
                ),
                |e| eprintln!("Getting record failed: {}", e),
            )
        )
    }
    fn set_done(&self, msg_id: String) -> () {
        if let Some(_) = try_or_else_none(
            || self.apply_mut(
                |db| db::process_message(
                    &db,
                    msg_id.as_str(),
                    &|msg: notmuch::Message<notmuch::Query>| {
                        msg.remove_tag("unread")?;
                        msg.remove_tag("inbox")?;
                        msg.tags_to_maildir_flags()?;
                        Ok(())
                    }
                )
            ),
            |e| eprintln!("Fetching record failed: {}, not marking as done", e),
        ) {
            self.emit_message_change(msg_id.clone());
        };
    }
    fn set_read(&self, msg_id: String) -> () {
        try_or_else(
            || self.apply_mut(
                |db| db::process_message(
                    &db,
                    msg_id.as_str(),
                    &|msg: notmuch::Message<notmuch::Query>| {
                        msg.remove_tag("unread")?;
                        msg.tags_to_maildir_flags()?;
                        Ok(())
                    }
                )
            ),
            |e| eprintln!("Fetching record failed: {}, not marking as read", e),
        );
        self.emit_message_change(msg_id.clone());
    }
    
    fn changed_message_id(&self) -> String {
        let data = self.data.borrow();
        data.last_changed_message_id.clone()
    }
    
    fn dirty(&self) -> bool {
        let data = self.data.borrow();
        let scanner = data.scanner.borrow();
        scanner.get_dirty()
    }
}

/// Used in the message view, so eagerly loaded
pub type MessageRecordArgs = Option<data::MessageRecord>;

impl MessageRecordTrait for MessageRecord {
    fn emit(&self) -> &MessageRecordEmitter {
        &self.emit
    }
    fn inbox(&self) -> bool {
        let record = self.data.borrow();
        match *record {
            Some(ref r) => r.inbox,
            None => false, // would be nice to return tri-state values
        }
    }
    fn unread(&self) -> bool {
        let record = self.data.borrow();
        match *record {
            Some(ref r) => r.unread,
            None => false, // would be nice to return tri-state values
        }
    }
}

/// The QML singleton represents a bookmark storage backend.
/// It's a QML object to allow other QML objects to express their bookmarks source
// Disallowing direct access to data will actually make this act as an arbiter against simultaneous access.
// That'll also allow the option to combine updating with storing.
pub type BookmarksDataArgs = persistence::Bookmarks;


impl BookmarksDataTrait for BookmarksData {
    fn emit(&self) -> &BookmarksDataEmitter {
        &self.emit
    }
}

use ::bookmarks::Bookmark;

// TODO: move get/set to BookmarksData, and allow this to be a dumb table concerned with representation only
/// Stores data behind the bookmarks table
pub struct Bookmarks {
    emit: BookmarksEmitter,
    model: BookmarksTree,
    base: Option<Rc<RefCell<Database>>>,
    storage: Option<Rc<RefCell<BookmarksData>>>,
    // Bookmarks contents is duplicated in queries to avoid borrowing the QML object and blocking
    // Access to queries include QML reloading cells, so it's frequent. It could also be networked in the future.
    queries: Vec<Bookmark>,
    tags: Vec<Bookmark>,
    generated: Vec<Bookmark>,
}

impl Bookmarks {
    fn get_view<'a>(&'a self)
            -> CombinedSlice<'a, Bookmark, CombinedSlice<'a, Bookmark, &'a Vec<Bookmark>>> {
        CombinedSlice::new(
            CombinedSlice::new(&self.tags, self.queries.as_slice()),
            self.generated.as_slice()
        )
    }
    fn save_bookmarks(&mut self) -> () {
        if let Some(ref storage) = self.storage {
            let storage = storage.borrow();
            storage.data.replace(persistence::Bookmarks {
                queries: self.queries.iter().filter_map(|b| {
                    match b {
                        Bookmark::Query(q) => Some(q.clone()),
                        _ => None
                    }
                }).collect::<Vec<_>>()
            });

            let bookmarks = storage.data.borrow();
            if let Err(e) = bookmarks.save() {
                eprintln!("Bookmarks failed to save");
            }
        } else {
            eprintln!("Not connected to any bookmarks!");
        }
    }
}

impl<'a> BookmarksTrait for Bookmarks {
    fn new(emit: BookmarksEmitter, model: BookmarksTree) -> Self {
        Bookmarks {
            emit: emit,
            model: model,
            base: None,
            storage: None,
            queries: Vec::new(),
            tags: Vec::new(),
            generated: vec![Bookmark::Uncategorized, Bookmark::UnreadUncategorized],
        }
    }
    fn database(&self) -> &Database {panic!();}
    fn database_mut(&mut self) -> Option<&mut Rc<RefCell<Database>>> {
        if let Some(ref mut db) = self.base {
            Some(db)
        } else {
            None
        }
    }
    fn set_database(&mut self, nmdatabase: Rc<RefCell<Database>>) {
        {
            let db = nmdatabase.borrow_mut();
            try_or_else(
                || {
                    db.apply(|db| {
                        self.tags = db
                            .all_tags().expect("Tags search failed")
                            .filter_map(Bookmark::from_tag_name)
                            .collect::<Vec<_>>();
                        Ok(())
                    })
                },
                |e| eprintln!("Failed to read tags from new db: {}", e)
            );
        }
        self.base = Some(nmdatabase);
        self.emit.database_changed();
        // Strictly speaking, only thelast 2 columns chnged,
        // but the interface doesn't expose columns, and it's cheap anyway.
        self.model.data_changed(
            0,
            self.tags.len() + self.queries.len() + self.generated.len() - 1
        );
    }
    fn clear_database(&mut self) {
        self.base = None;
        self.emit.database_changed();
    }
    fn storage(&self) -> &BookmarksData {panic!();}
    fn storage_mut(&mut self) -> Option<&mut Rc<RefCell<BookmarksData>>> {
        if let Some(ref mut s) = self.storage {
            Some(s)
        } else {
            None
        }
    }
    fn set_storage(&mut self, bdata: Rc<RefCell<BookmarksData>>) {
        let bookmarks_data = bdata.borrow();
        let bookmarks = (*bookmarks_data).data.borrow();
        self.queries = bookmarks
            .queries
            .clone()
            .into_iter()
            .map(Bookmark::Query)
            .collect::<Vec<_>>();
        self.storage = Some(bdata.clone());
        self.emit.database_changed();
    }
    fn clear_storage(&mut self) { panic!("TODO"); }

    fn emit(&mut self) -> &mut BookmarksEmitter {
        &mut self.emit
    }
    fn row_count(&self, _item: Option<usize>) -> usize {
        self.get_view().len()
    }
    fn index(&self, _item: Option<usize>, row: usize) -> usize {
        row
    }
    fn parent(&self, _item: usize) -> Option<usize> {
        None
    }
    fn row(&self, index: usize) -> usize {
        index
    }
    fn check_row(&self, index: usize, _row: usize) -> Option<usize> {
        if index < self.get_view().len() {
            Some(self.row(index))
        } else {
            None
        }
    }
   
    fn unread_count(&self, index: usize) -> u32 {
        self.base.as_ref()
            .and_then(|base| {
                let base = base.borrow();
                base
                    .apply_generic(
                        |db| {
                            db.create_query(&format!("({}) and tag:unread", self.get_query(index as u32)))
                                .and_then(|q| q.count_messages())
                        },
                        notmuch::DatabaseMode::ReadOnly,
                    )
                    .warn_ok()
            })
            .unwrap_or(0)
    }
    
    fn waiting_count(&self, index: usize) -> u32 {
        self.base.as_ref()
            .and_then(|base| {
                let base = base.borrow();
                base
                    .apply_generic(
                        |db| {
                            db.create_query(&format!("({}) and tag:inbox", self.get_query(index as u32)))
                                .and_then(|q| q.count_messages())
                        },
                        notmuch::DatabaseMode::ReadOnly,
                    )
                    .warn_ok()
            })
            .unwrap_or(0)
    }
    
    fn name(&self, item: usize) -> &str {
        self.get_view()
            .get(item)
            .expect("Out of bounds")
            .to_name()
    }
    fn type_(&self, item: usize) -> u32 {
        self.get_view().get(item).expect("Out of bounds")
            .get_type_num()
    }
    fn get_query(&self, index: u32) -> String {
        use ::bookmarks;
        let b = bookmarks::Collection(self.queries.as_slice());
        b.to_query(
            self.get_view()
                .get(index as usize).expect("Out of bounds")
                .clone()
        ).0
    }
    fn add_query(&mut self, name: String, query: String) -> () {
        // TODO: sorting
        let idx = self.tags.len() + self.queries.len();
        self.model.begin_insert_rows(None, idx, idx);
        self.queries.push(Bookmark::Query(persistence::Query{ name, query }));
        self.model.end_insert_rows();
        self.model.data_changed(
            self.tags.len() + self.queries.len(),
            self.tags.len() + self.queries.len() + self.generated.len() - 1
        );
        self.save_bookmarks();
    }
    fn delete_query(&mut self, id: String) -> () {
        let q_idx = self.queries.iter().position(|b|
            match b {
                Bookmark::Query(persistence::Query{name: _, ref query}) => {
                    query == &id
                },
                _ => false
            }
        );
        match q_idx {
            None => eprintln!("Tried to delete a query that doesn't exist"),
            Some(q_idx) => {
                let idx = self.tags.len() + q_idx;
                self.model.begin_remove_rows(None, idx, idx);
                self.queries.remove(q_idx);
                self.model.end_remove_rows();
                self.model.data_changed(
                    idx,
                    self.tags.len() + self.queries.len() + self.generated.len() - 1
                );
                self.save_bookmarks();
            }
        }
    }
    
    fn handle_message_change(&mut self, id: String) -> () {
        println!("Message with id {} changed", id);
        // TODO: update message counts
    }
}

/// Displays message contents
pub struct MessageLoader {
    emit: MessageLoaderEmitter,
    message: Option<data::CachedMessage>,
    date: Option<String>,
    filename: String, // TODO: make option
}

impl MessageLoaderTrait for MessageLoader {
    fn new(emit: MessageLoaderEmitter) -> Self {
        MessageLoader { emit: emit,
                        message: None,
                        date: None,
                        filename: String::new() }
    }
    fn emit(&mut self) -> &mut MessageLoaderEmitter {
        &mut self.emit
    }
    fn body(&self) -> &str {
        match self.message {
            Some(ref msg) => msg.get_body(),
            None => ""
        }
    }
    fn date(&self) -> &str {
        match self.date {
            Some(ref d) => d,
            None => ""
        }
    }
    fn filename(&self) -> &str {
        &self.filename
    }
    fn set_filename(&mut self, value: String) {
        self.filename = value;
        self.emit.filename_changed();
        
        if self.filename.is_empty() {
            self.message = None;
            self.date = None;
        } else {
            self.message = match data::CachedMessage::new(self.filename.clone().into()) {
                Ok(m) => Some(m),
                Err(e) => {
                    eprintln!("Message could not be read: {:?}", e);
                    None
                },
            };

            self.date = match self.message {
                Some(ref msg) => Some(format!("{}", msg.meta.date)), // TODO: use a formatter dependent on the settings/current time?
                None => None,
            }
        }
        self.emit.msg_id_changed();
        self.emit.body_changed();
        self.emit.date_changed();
        self.emit.sender_changed();
        self.emit.recipients_changed();
        self.emit.subject_changed();
        self.emit.raw_headers_changed();
    }
    fn sender(&self) -> &str {
        match self.message {
            Some(ref msg) => match msg.meta.sender {
                Some(ref s) => &s,
                None => "",
            },
            None => ""
        }
    }
    fn recipients(&self) -> &str {
        match self.message {
            Some(ref msg) => match msg.meta.recipients {
                Some(ref r) => &r,
                None => "",
            },
            None => ""
        }
    }
    fn subject(&self) -> &str {
        match self.message {
            Some(ref msg) => match msg.meta.subject {
                Some(ref s) => &s,
                None => "",
            },
            None => ""
        }
    }
    fn msg_id(&self) -> &str {
        match self.message {
            Some(ref msg) => match msg.meta.msg_id {
                Some(ref s) => &s,
                None => "",
            },
            None => ""
        }
    }
    fn raw_headers(&self) -> &str {
        match self.message {
            Some(ref msg) => &msg.raw_headers,
            None => ""
        }
    }
}

/// Threaded message metadata
pub struct MessageMetaArgs {
    message: data::DisplayMessageMeta,
    peers: String,
}

impl MessageMetaTrait for MessageMeta {
    fn emit(&self) -> &MessageMetaEmitter {
        &self.emit
    }
    fn depth(&self) -> u32 {
        let data = self.data.borrow();
        data.message.depth as u32
    }
    fn peers(&self) -> String {
        let data = self.data.borrow();
        data.peers.clone()
    }
    fn unread(&self) -> bool {
        let data = self.data.borrow();
        data.message.unread
    }
}

/// Model for data stored in a message list
pub struct Messages {
    emit: MessagesEmitter,
    model: MessagesTree,
    base: Option<Rc<RefCell<Database>>>,
    query: String,
    query_validity: bool,
// TODO: group filter and its validity with the IDs to create a single point to check for their existence
    filter: String,
    filter_validity: bool,
    messages: Vec<data::DisplayMessageMeta>,
    filtered_ids: HashSet<String>,
}

/// Message info from Notmuch
struct ThreadedMessageMeta {
    id: String,
    path: PathBuf,
    depth: u32,
    unread: bool,
}

fn descend<'o, O: 'o + MessageOwner, T: Iterator<Item=Message<'o, O>>>(iter: T, depth: u32)
        -> Vec<ThreadedMessageMeta> {
    iter.flat_map(|m| {
        let mut v = vec!(ThreadedMessageMeta {
            // FIXME: sort by date, see gitlab_p
            id: m.id().to_string(),
            path: m.filename(),
            depth: depth,
            unread: m.tags().any(|x| x == "unread"),
        });
        v.extend(descend(m.replies(), depth + 1));
        v
    }).collect::<Vec<_>>()
}

struct ThreadMeta {
    subject: String,
    date: i64,
    messages: Vec<ThreadedMessageMeta>,
}

impl Messages {
    /// Synchronizes the state of the model with the query
    fn update(&mut self) {
        let new_messages = if self.query.is_empty() {
            Some(Vec::new())
        } else {
            try_or_else_none(
                || {
                    if let Some(ref db) = self.base {
                        let db = db.borrow();
                        let db = db.get()
                            .map_err(|e| db::Error::Poisoned(format!("Database on Messages poisoned: {}", e)))?;
                        let query = db.create_query(&self.query)?;
                        let threads = query.search_threads();
                        
                        self.query_validity = threads.is_ok(); // TODO: save error message
                        self.emit.query_valid_changed(); // FIXME: only if different
                        if !self.query_validity {
                            return Err(db::Error::TextError("Invalid query"));
                        }
                        
                        let mut threads = threads?
                            .map(|t| ThreadMeta {
                                subject: t.subject().to_string(),
                                date: t.newest_date(),
                                messages: descend::<::notmuch::Thread, ::notmuch::Messages<::notmuch::Thread>>(t.toplevel_messages(), 0)
                            })
                            .collect::<Vec<_>>();

                        threads.sort_by_key(|t: &ThreadMeta| t.date);

                        Ok(threads.into_iter().flat_map(|t: ThreadMeta| {
                            let subject = t.subject;
                            t.messages.into_iter()
                                .map(move |tmm| data::DisplayMessageMeta::new(
                                    tmm.id,
                                    tmm.path,
                                    tmm.depth,
                                    //thread_date: date.clone(),
                                    subject.clone(),
                                    tmm.unread,
                                ))
                        }).collect::<Vec<_>>())
                    } else {
                        Err(db::Error::TextError("No database on messages"))
                    }
                },
                |e| eprintln!("Failed to update: {}", e) // TODO: place errors in the right spots depending on criticality
            )
        };
        if let Some(messages) = new_messages {
            self.model.begin_reset_model();
            self.messages = messages;
            self.model.end_reset_model();
//            self.model.data_changed(0, self.messages.len());
        }
    }

    fn update_filter(&mut self) {
        if !self.query_validity | self.messages.is_empty() {
            self.filter_validity = true;
            self.emit.filter_valid_changed();
            return;
        }
        let new_filtered_ids = try_or_else_none(
            || {
                if self.filter.is_empty() {
                    Ok(HashSet::new())
                } else if let Some(ref db) = self.base {
                    let db = db.borrow();
                    let db = db.get()
                        .map_err(|e| db::Error::Poisoned(format!("Database on Messages poisoned: {}", e)))?;
                    let query = db.create_query(&format!("{} and {}", self.query, self.filter))?;
                    let messages = query.search_messages();
                    // Invalid queres don't allow filtering, so any mistake here can be chalked up to the filter
                    self.filter_validity = messages.is_ok(); // TODO: save error message
                    self.emit.filter_valid_changed(); // FIXME: only if different
                    if !self.filter_validity {
                        return Err(db::Error::TextError("Invalid filter"));
                    }
                    let strings: HashSet<_> = messages?
                        .map(|m| m.id().to_string())
                        .collect();
                    Ok(strings)
                } else {
                    Err(db::Error::TextError("No database on messages"))
                }
            },
            |e| eprintln!("Failed to update: {}", e) // TODO: place errors in the right spots depending on criticality
        );
        
        // when an error happens, updating should be skipped. There's no need to change what's displayed, except the explicit error message.
        
        if let Some(new_filtered_ids) = new_filtered_ids {
            let indices: Vec<_> = {
                let changed_ids: HashSet<_> = self.filtered_ids
                    .symmetric_difference(&new_filtered_ids)
                    .collect();

                // TODO: consider saving the filtering outcomes already
                self.messages
                    .iter()
                    .enumerate()
                    .filter(|(_idx, e)| changed_ids.contains(&e.get_id()))
                    .map(|(idx, _e)| idx)
                    .collect()
            };
            
            // Assignment of new IDs must occur before the data_changed signal is issued for the data to be available as the view is refreshed
            self.filtered_ids = new_filtered_ids;
            // TODO: benchmark different strategies: coalescing consecutive, coalescing gaps, refreshing all
            //indices.iter().for_each(|i| self.model.data_changed(*i, i + 1));
            self.model.data_changed(0, self.messages.len());
        }
    }
}

impl MessagesTrait for Messages {
    fn new(emit: MessagesEmitter, model: MessagesTree) -> Self {
        Messages { emit: emit,
                   model: model,
                   base: None,
                   query: String::new(),
                   query_validity: true,
                   filter: String::new(),
                   filter_validity: true,
                   messages: Vec::new(),
                   filtered_ids: HashSet::new() }
    }
    fn emit(&mut self) -> &mut MessagesEmitter {
        &mut self.emit
    }
    fn query(&self) -> &str {
        &self.query
    }
    fn set_query(&mut self, value: String) {
        self.query = value;
        self.update();
        self.emit.query_changed();
    }
    fn filter(&self) -> &str {
        &self.filter
    }
    fn set_filter(&mut self, value: String) {
        self.filter = value;
        self.update_filter();
        self.emit.filter_changed();
    }
    fn query_valid(&self) -> bool {
        self.query_validity
    }
    fn filter_valid(&self) -> bool {
        self.filter_validity
    }
    fn row_count(&self, _item: Option<usize>) -> usize {
        self.messages.len()
    }
    fn index(&self, _item: Option<usize>, row: usize) -> usize {
        row
    }
    fn parent(&self, _item: usize) -> Option<usize> {
        None
    }
    fn row(&self, item: usize) -> usize {
        item
    }
    fn check_row(&self, index: usize, _row: usize) -> Option<usize> {
        if index < self.messages.len() {
            Some(self.row(index))
        } else {
            None
        }
    }
    fn date(&self, item: usize) -> String {
        format!("{}", self.messages[item].get_date().into_utc())
    }
    // TODO: is this even needed?
    fn subject(&self, item: usize) -> String {
        self.messages[item].get_header("subject")
            .unwrap_or(Some("Error".to_string())) // retrieval failed
            .unwrap_or("Empty".to_string())
    }
    fn thread_subject(&self, item: usize) -> &str {
        &self.messages[item].thread_subject
    }
    fn filename(&self, item: usize) -> &str {
        self.messages[item].path.to_str().expect("Filename not decodeable")
    }
    fn get_filename(&self, index: u32) -> String {
        self.filename(index as usize).into()
    }
    fn filtered(&self, item: usize) -> u32 {
        return self.filtered_ids.contains(&self.messages[item].get_id()) as u32
    }
    
    fn database(&self) -> &Database { panic!("implement database"); }
    fn database_mut(&mut self) -> Option<&mut Rc<RefCell<Database>>> {
        if let Some(ref mut db) = self.base {
            Some(db)
        } else {
            None
        }
    }
    fn set_database(&mut self, database: Rc<RefCell<Database>>) {
        self.base = Some(database.clone());
        self.update();
        self.emit.database_changed();
    }
    fn clear_database(&mut self) {
        self.base = None;
        self.model.begin_reset_model();
        self.messages = vec!();
        self.model.end_reset_model();
        self.model.data_changed(0, self.messages.len());
        self.emit.database_changed();
    }
    fn get_message(&self, index: u32) -> Rc<RefCell<MessageMeta>> {
        let message = self.messages[index as usize].clone();
        let peers = message.get_header("from")
            .unwrap_or(Some("Error".to_string())) // retrieval failed
            .unwrap_or("Empty".to_string());
        MessageMeta::create(MessageMetaArgs {
            message: message,
            peers: peers,
        })
    }
    fn handle_message_change(&mut self, id: String) -> () {
        println!("Messages handle id {} change", id);
        // TODO: search for the id in the message list and update its properties
        // if not found in the message list, make a query with the id
        // and see if it's in results
        self.update();
    }

}
