cmake_policy(SET CMP0048 NEW)
project(quemail
    VERSION 0.1.2
    DESCRIPTION "Reads a deluge of emails"
)

cmake_minimum_required(VERSION 2.8.12 FATAL_ERROR)
cmake_policy(SET CMP0046 NEW)
cmake_policy(SET CMP0063 NEW)
cmake_policy(SET CMP0071 NEW)

LIST(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DQT_QML_DEBUG ")

string(TOUPPER "${CMAKE_BUILD_TYPE}" CMAKE_BUILD_TYPE_UPPER)
if(CMAKE_BUILD_TYPE_UPPER STREQUAL RELEASE)
    set(RUST_TARGET_DIR release/)
    set(RUST_BUILD_FLAG --release)
else()
    set(RUST_TARGET_DIR debug/)
    set(RUST_BUILD_FLAG)
endif()

### find dependencies ###

include(FeatureSummary)
find_package(Cargo REQUIRED)
find_package(Rust REQUIRED)
find_package(rqbg "0.5.0" REQUIRED)
set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
find_package(Threads REQUIRED)

set(QT_MIN_VERSION "5.6.0")
find_package(Qt5 ${QT_MIN_VERSION} CONFIG
    REQUIRED COMPONENTS Core Quick Widgets
)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)

### build commands ###

SET(CARGO_TARGET_DIR "${CMAKE_BINARY_DIR}/rust")

SET(RUST_DIR "${CMAKE_CURRENT_SOURCE_DIR}/rust")
SET(RUST_LIB "${CARGO_TARGET_DIR}/${RUST_TARGET_DIR}/librust.a")

list(APPEND RustDeps
    rust/src/data.rs
    rust/src/db.rs
    rust/src/ccell.rs
    rust/src/lib.rs
    rust/src/persistence.rs
    rust/src/qml.rs
    rust/src/utils.rs
    rust/Cargo.toml
)

foreach(FILE IN LISTS RustDeps)
    configure_file("${FILE}" "${FILE}" COPYONLY)
endforeach()

# compile the rust code into a static library
add_custom_command(
  OUTPUT "${RUST_LIB}"
  COMMAND env CARGO_TARGET_DIR="${CARGO_TARGET_DIR}" "${Cargo_EXECUTABLE}" build ${RUST_BUILD_FLAG}
  DEPENDS ${RustDeps}
          "${CARGO_TARGET_DIR}/src/interface.rs"
  WORKING_DIRECTORY "${RUST_DIR}"
)

add_custom_target(
  test
  COMMAND env CARGO_TARGET_DIR="${CARGO_TARGET_DIR}" "${Cargo_EXECUTABLE}" test
  DEPENDS "${RUST_LIB}"
  WORKING_DIRECTORY "${RUST_DIR}"
)

add_custom_command(
  OUTPUT "${CARGO_TARGET_DIR}/src/interface.rs" "${CARGO_TARGET_DIR}/bindings.h" "${CARGO_TARGET_DIR}/bindings.cpp"
  COMMAND "${rqbg_EXECUTABLE}" --destination . "${CMAKE_CURRENT_SOURCE_DIR}/bindings.yaml"
  DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/bindings.yaml"
  WORKING_DIRECTORY "${CARGO_TARGET_DIR}"
)

add_custom_target(rust_target DEPENDS "${RUST_LIB}")

list(APPEND Libs "${RUST_LIB}")
list(APPEND Libs Qt5::Core Qt5::Quick Qt5::Widgets ${CMAKE_DL_LIBS} notmuch pthread)
set(SRCS
  src/main.cpp
  "${CARGO_TARGET_DIR}/bindings.cpp"
  "${CARGO_TARGET_DIR}/bindings.h")
add_executable(quemail ${SRCS})
add_dependencies(quemail rust_target)
target_link_libraries(quemail ${Libs})
set_target_properties(quemail PROPERTIES
    CXX_STANDARD 11
    CXX_STANDARD_REQUIRED ON
)

install(TARGETS
    quemail
)

# Install data

include(GNUInstallDirs)

set(QUEMAIL_DATA "${CMAKE_INSTALL_DATADIR}/quemail")
add_compile_definitions(QML_DIR="${CMAKE_INSTALL_PREFIX}/${QUEMAIL_DATA}/ui")

install(FILES
  ui/main.qml
  ui/MessageItem.qml
  ui/MessageViewer.qml
  ui/QueryInput.qml
  ui/SelectableLabel.qml
  DESTINATION "${QUEMAIL_DATA}/ui"
)
