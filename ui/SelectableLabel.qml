import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4

// FIXME/workaround: Text doesn't line up with Label when selected
Item {
    property alias text: t.text
    height: t.height - 10 // reduce height by margin size
    TextField {
        id: t
        // stretches the TextField and positions it at an offset
        // black magic: it should cancel out and leave just the height, but doesn't work with low values
        // FIXME: using too big margins causes mouse field to be offset
        anchors.topMargin: -5
        anchors.bottomMargin: -5
        anchors.fill: parent
        readOnly: true
        style: TextFieldStyle {
            background: Item {}
        }
    }
}
