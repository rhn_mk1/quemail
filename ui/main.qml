/*
 * Copyright (C) 2014 Sergei Shilovsky <sshilovsky@gmail.com>
 * Copyright (C) 2018 rhn <gihu.rhn@porcupinefactory.org>
 *
 * SPDX-License-Identifier: GPL-3+
 */

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls 1.4 as Controls
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import Quemail 1.0
import org.kde.kirigami 2.4 as Kirigami

ApplicationWindow {
    visibility: "Maximized"
    visible: true
    title: qsTr("Quemail?")

    Storage {
        id: storage
    }

    header: ToolBar {
        position: ToolBar.Header
        Layout.fillHeight: false
        Layout.fillWidth: true
        RowLayout {
            ToolButton {
                text: "Update"
                enabled: storage.database.dirty
                onClicked: storage.update();
            }
        }
    }


    SplitView {
        id: splitView
        Layout.fillHeight: true
        Layout.fillWidth: true
        anchors.fill: parent
        orientation: Qt.Horizontal

        ColumnLayout {
            id: columnLayout
            width: 200

            RowLayout {
                TextField {
                    id: queryName
                    placeholderText: "name"
                    Layout.fillWidth: true

                    Keys.onReturnPressed: {
                        queries.model.addQuery(queryName.text, qi.queryString)
                        queryName.text = "";
                    }
                }

                Button {
                    id: querySave
                    focus: false
                    text: "Save"
                    onClicked: {
                        queries.model.addQuery(queryName.text, qi.queryString)
                        queryName.text = "";
                    }
                }
            }

            TableView {
                id: queries
                Layout.fillHeight: true
                width: 250
                Layout.fillWidth: true
                model: Bookmarks {
                    database: storage.database
                    storage: storage.bookmarks_data
                }
                Connections {
                    target: storage.database
                    onChanged_message_idChanged: {
                        queries.model.handleMessageChange(
                            storage.database.changed_message_id
                        );
                    }
                }
                property string selection: null
                onClicked: function(row) {
                    selection = model.getQuery(row)
                }

                onSelectionChanged: {
                    qi.queryString = selection
                    queryFilter.text = ""
                }

                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.RightButton
                    onClicked: function(mouse) {
                        contextMenu.row = queries.rowAt(mouse.x, mouse.y)
                        if (contextMenu.row >= 0) {
                            contextMenu.popup()
                        }
                    }
                }

                Controls.Menu {
                    id: contextMenu
                    property int row: -1
                    Controls.MenuItem {
                        action: Controls.Action {
                            text: "&Delete"
                            iconName: "edit-delete"
                            shortcut: StandardKey.Delete
                            onTriggered: {
                                var query = queries.model.getQuery(contextMenu.row)
                                queries.model.deleteQuery(query)
                            }
                        }
                    }
                }

                TableViewColumn {
                    role: "type_"
                    title: "type"
                    width: 30
                    delegate: Label {
                        text: styleData.value /*{
                            "0": "T", // tag
                            "1": "U", // user defined
                            "2": "D", // directory
                            "3": "G", // generated
                        }[styleData.value]*/
                    }
                }

                TableViewColumn {
                    role: "name"
                    title: "name"
                    width: 120
                }

                TableViewColumn {
                    role: "unread_count"
                    title: "unread"
                    width: 70
                }

                TableViewColumn {
                    role: "waiting_count"
                    title: "waiting"
                    width: 70
                }
            }
        }

        ColumnLayout {
            QueryInput {
                id: qi
                Layout.fillWidth: true
                valid: messageList.model.queryValid
            }

            RowLayout {
                Label {
                    text: "and"
                }

                Controls.TextField {
                    id: queryFilter
                    property bool valid: messageList.model.filterValid
                    textColor: {
                        if (valid) {
                            Kirigami.Theme.textColor
                        } else {
                            Kirigami.Theme.negativeTextColor
                        }
                    }
                    placeholderText: "filter"
                    Layout.fillWidth: true
                }
            }

            SplitView {
                id: splitView2
                orientation: Qt.Vertical
                Layout.fillHeight: true
                Layout.fillWidth: true

                TableView {
                    id: messageList
                    height: 232.5
                    flickableItem.boundsBehavior: Flickable.StopAtBounds

                    ColumnLayout {
                        id: placeholder
                        Label {
                            id: line
                            text: "a"
                        }
                        visible: false
                    }

                    Component {
                        id: sectionHeading
                        Label {
                            text: "  " + section
                            font.capitalization: Font.SmallCaps
                            font.italic: true
                            font.pointSize: line.font.pointSize * 1.3
                            height: placeholder.height * 1.7
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignBottom
                        }
                    }

                    section.property: "threadSubject"
                    section.delegate: sectionHeading

                    TableViewColumn {
                        role: "filtered"
                        title: "filter"
                        width: placeholder.width * 2
                        delegate: Label {
                            text: styleData.value ? styleData.value : ""/*{
                                "0": "", // Not matched
                                "1": "X", // matched
                                "": "", // header
                            }[styleData.value ? styleData.value : ""]*/
                        }
                    }

                    TableViewColumn {
                        role: "filename"
                        title: "message"
                        delegate: MessageItem {
                            model: messageList.model;
                        } // TODO: hold inside a Loader
                        width: placeholder.width * 80
                    }

                    TableViewColumn {
                        role: "date"
                        title: "date"
                    }

                    rowDelegate: Rectangle { // to double the height
                        Layout.fillWidth: true
                        height: placeholder.height * 1.1

                        SystemPalette {
                            id: myPalette;
                            colorGroup: SystemPalette.Active
                        }
                        color: {
                            var baseColor = styleData.alternate?myPalette.alternateBase:myPalette.base
                            return styleData.selected?myPalette.highlight:baseColor
                        }
                    }

                    model: Messages {
                        database: storage.database
                        query: qi.queryString
                        filter: queryFilter.text
                        onModelReset: {
                            if (rowCount() > 0) {
                                var lastRow = rowCount() - 1
                                messageList.positionViewAtRow(lastRow, ListView.Contain)
                                messageList.selection.select(lastRow)
                                // surprisingly, choosing the current row doesn't set the selection
                                messageList.currentRow = lastRow
                            }
                        }
                    }

                    Connections {
                        target: storage.database
                        onChanged_message_idChanged: {
                            messageList.model.handleMessageChange(storage.database.changed_message_id);
                        }
                    }

                    onCurrentRowChanged: {
                        // Workaround: moving selection upwards with arrows disappears it
                        positionViewAtRow(currentRow, ListView.Contain)
                    }
                }

                MessageViewer {
                    id: messageViewer
                    database: storage.database
                    messagePath: {
                        if (messageList.currentRow >= 0) {
                            messageList.model.getFilename(messageList.currentRow)
                        } else {
                            ""
                        }
                    }
                }
            }
        }
    }
}
