*Quemail?* is made to sort a deluge of emails
=============================================

*Quemail?* is free software, released under the GNU General Public License version 3 (or later).

Status: work-in-progress.

*Quemail?* overview
-------------------

*Quemail?* came from my need to deal with the influx of emails, which I'm no longer able to deal with in a timely manner using Claws mail. While Claws has a good GUI, some of its basic ideas make it hard to correlate emails to each other, or assess their importance.

Currently, *Quemail?* is focusing on querying emails to get to the information that's relevant, and solving some issues.

- IMAP folders require manual management, while queries soak up emails immediately
- Saving queries should not be much harder than adding IMAP folders
- Tagging emails should be the main way of moving them from one category to another
- Some categories are are important than others
- Searching messages should be more powerful: full-text search for conversations with a person should be easy

### Features so far

- saved queries
- automatic queries
- displays threads in tree shape
- displays simple messages in text form

### Wishlist

- show CC/BCC
- show message's tags
- show some tags in the list
- read/acted tags (unread/inbox?)
- indicate important messages (unread/unacted)
- message composition
- enough integration to pick up new messages as they come
- decryption of PGP messages
- separate index when including encrypted data
- multiple accounts
- Markdown MIME type support
- attachments

### Hatelist

- no HTML tag rendering
- no JS
- mouse first, keyboard second; if this hurts touch, then too bad
- scrollbars will stay
- always respects system theme
- no more C++ than necessary

Technical stuff
---------------

*Quemail?* is using a [patched version](https://gitlab.com/rhn_mk1/rust-qt-bindings-generator) of the [Rust Qt bindings generator](https://phabricator.kde.org/source/rust-qt-binding-generator/). Unfortunately, my contributions were rejected on technical grounds.

### Building

```
cd quemail
mkdir ../quemail-build
cd ../quemail-build
cmake ../quemail -DCMAKE_INSTALL_PREFIX=../quemail-install
make
make install
```

#### Updating Cargo dependencies

```
cd quemail/rust
CARGO_TARGET_DIR=../../quemail-build/rust cargo update -p foo
cd ../../quemail-build/
touch rust/rust/src/interface.rs # needed to trigger Rust bindings rebuild
make
```

### Running

`$MAILDIR` should contain a path to notmuch mail database. If not set, `$HOME/mail` is used (TODO).

When using Claws mail, set to `~/.claws-mail/imapcache/$SERVER/$ACCOUNT/`, e.g.:

```
MAILDIR=~/.claws-mail/imapcache/imap.porcupinefactory.org/gihu.rhn@porcupinefactory.org/ ./quemail-install/bin/quemail
```

Contributing
------------

Pull requests welcome.

Contact
-------

rhn <gihu.rhn@porcupinefactory.org>
