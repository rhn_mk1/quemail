/*
 * Copyright (C) 2014 Sergei Shilovsky <sshilovsky@gmail.com>
 * Copyright (C) 2018 rhn <gihu.rhn@porcupinefactory.org>
 *
 * SPDX-License-Identifier: GPL-3+
 */

#include <QtQml>
#include <QApplication>
#include <QProcessEnvironment>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "rust/bindings.h"

int main(int argc, char *argv[]) {
    qmlRegisterType<Storage>("Quemail", 1, 0, "Storage");
    qmlRegisterType<Database>("Quemail", 1, 0, "Database");
    qmlRegisterType<Bookmarks>("Quemail", 1, 0, "Bookmarks");
    qmlRegisterType<MessageLoader>("Quemail", 1, 0, "MessageLoader");
    qmlRegisterType<Messages>("Quemail", 1, 0, "Messages");
    qmlRegisterType<MessageMeta>("Quemail", 1, 0, "MessageMeta");
    qmlRegisterType<MessageRecord>("Quemail", 1, 0, "MessageRecord");

    QApplication app(argc, argv); // TODO maybe switch to QGuiApplication
    QQmlApplicationEngine engine; // TODO maybe switch to QQmlEngine
    QString qml_root = QProcessEnvironment::systemEnvironment().value("QML_DATA_DIR", QML_DIR) + "/main.qml";

    engine.load(qml_root);

    return app.exec();
}
